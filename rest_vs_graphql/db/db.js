const todos = [
    {
        id: 1,
        title: "Node.js",
        description: "Learn Node.js",
        priority: "URGENT",
        difficulty: "EASY",
        status: "IN_PROGRESS"
    },
    {
        id: 2,
        title: "Express",
        description: "Learn Express",
        priority: "HIGH",
        difficulty: "EASY",
        status: "IN_PROGRESS"
    },
    {
        id: 3,
        title: "GraphQL",
        description: "Learn GraphQL",
        priority: "URGENT",
        difficulty: "EASY",
        status: "IN_PROGRESS"
    },
    {
        id: 4,
        title: "Jest",
        description: "Learn Jest",
        priority: "MEDIUM",
        difficulty: "EASY",
        status: "TODO"
    },
    {
        id: 5,
        title: "Mandarin",
        description: "Learn Mandarin",
        priority: "LOW",
        difficulty: "NORMAL",
        status: "TODO"
    },
    {
        id: 6,
        title: "Keep calm",
        description: "Keep calm",
        priority: "URGENT",
        difficulty: "HARD",
        status: "DONE"
    }
];

let id = todos.length + 1;

module.exports = { todos, id };
