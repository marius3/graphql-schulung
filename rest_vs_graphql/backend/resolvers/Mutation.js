const mutation = {
    createTodo(parent, args, { db }, info) {

        const todo = {
            id: db.id++,
            title: args.data.title,
            description: args.data.description,
            priority: args.data.priority,
            difficulty: args.data.difficulty,
            status: args.data.status
        };

        db.todos.push(todo);

        return todo;
    },

    deleteTodo(parent, args, { db }, info) {

        const todoIndex = db.todos.findIndex((todo) => todo.id === args.id);

        if (todoIndex === -1) {
            throw new Error('Todo not found');
        }

        const deletedTodos = db.todos.splice(todoIndex, 1);

        return deletedTodos[0];
    },

    updateTodo(parent, args, { db }, info) {

        const todoIndex = db.todos.findIndex((todo) => todo.id === args.id);

        if (todoIndex === -1) {
            throw new Error('Todo not found');
        }

        const todo = db.todos[todoIndex];

        Object.keys(args.data).forEach(key => {
            if (args.data[key]) {
                todo[key] = args.data[key];
            }
        });

        return todo;
    }
}

module.exports = mutation;
