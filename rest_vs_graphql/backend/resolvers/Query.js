const query = {
    todos(parent, args, { db }, info) {

        const keys = Object.keys(args);

        if (keys.length === 0) {
            return db.todos;
        }

        let result = db.todos;
        let value;
        
        keys.forEach(key => {
            value = args[key];
            if (key === 'search') {
                result = result.filter(todo => [todo.id, todo.title, todo.description].toString().toLowerCase().includes(value.toLowerCase()));
            } else {
                result = result.filter(todo => todo[key].toLowerCase() === value.toLowerCase());
            }
        });

        return result;
    }
}

module.exports = query;
