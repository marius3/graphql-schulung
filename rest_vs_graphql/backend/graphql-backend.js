const { GraphQLServer } = require('graphql-yoga');
const db = require('../db/db');

const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');

const server = new GraphQLServer({
    typeDefs: './backend/schema.graphql',
    resolvers: {
        Query,
        Mutation
    },
    context: {
        db
    }
});

server.start(() => {
    console.log('Server is up!');
})
