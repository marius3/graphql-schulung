const express = require('express');
const bodyParser = require('body-parser');
const db = require('../db/db');

const app = express();
app.set('json spaces', 4);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'null');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.get('/api/todos', (req, res) => {
    const keys = Object.keys(req.query);

    if (keys.length === 0) {
        res.status(200).json(db.todos);
    } else {
        let result = db.todos;
        let value;
        
        keys.forEach(key => {
            value = req.query[key];
            if (key === 'search') {
                result = result.filter(todo => [todo.id, todo.title, todo.description].toString().toLowerCase().includes(value.toLowerCase()));
            } else {
                result = result.filter(todo => todo[key].toLowerCase() === value.toLowerCase());
            }
        });

        return res.status(200).json(result);
    }
});

app.post('/api/todos', (req, res) => {

    if (!req.body.title) {
        return res.status(400).send({
            success: 'false',
            message: 'title is required'
        });
    } else if (!req.body.description) {
        return res.status(400).send({
            success: 'false',
            message: 'description is required'
        });
    }

    const todo = {
        id: db.id++,
        title: req.body.title,
        description: req.body.description,
        priority: req.body.priority,
        difficulty: req.body.difficulty,
        status: req.body.status
    }

    db.todos.push(todo);

    return res.status(201).send({
        success: 'true',
        message: 'to-do added successfully',
        todo
    })
});

app.get('/api/todos/:id', (req, res) => {

    const id = parseInt(req.params.id, 10);
    let result;

    for (let todo of db.todos) {
        if (todo.id === id) {
            result = todo;
            break;
        }
    }

    if (result) {
        return res.status(200).json(result);
    } else {
        return res.status(404).send({
            success: 'false',
            message: 'to-do does not exist',
        });
    }
});

app.delete('/api/todos/:id', (req, res) => {

    const id = parseInt(req.params.id, 10);
    let isDeleted = false;

    for (let todo of db.todos) {
        if (todo.id === id) {
            db.todos.splice(db.todos.indexOf(todo), 1);
            isDeleted = true;
            break;
        }
    }

    if (isDeleted) {
        return res.status(200).send({
            success: 'true',
            message: 'Todo deleted successfuly',
        });
    } else {
        return res.status(404).send({
            success: 'false',
            message: 'todo not found',
        });
    }

});

app.put('/api/todos/:id', (req, res) => {

    const id = parseInt(req.params.id, 10);

    let foundTodo;
    let itemIndex;

    for (let todo of db.todos) {
        if (todo.id === id) {
            foundTodo = todo;
            itemIndex = db.todos.indexOf(todo);
            break;
        }
    }
  
    if (!foundTodo) {
      return res.status(404).send({
        success: 'false',
        message: 'todo not found',
      });
    }
  
    if (!req.body.title) {
      return res.status(400).send({
        success: 'false',
        message: 'title is required',
      });
    } else if (!req.body.description) {
      return res.status(400).send({
        success: 'false',
        message: 'description is required',
      });
    }
  
    const updatedTodo = {
      id: foundTodo.id,
      title: req.body.title || foundTodo.title,
      description: req.body.description || foundTodo.description,
      priority: req.body.priority || foundTodo.priority,
      difficulty: req.body.difficulty || foundTodo.difficulty,
      status: req.body.status || foundTodo.status
    };
  
    db.todos.splice(itemIndex, 1, updatedTodo);
  
    return res.status(201).send({
      success: 'true',
      message: 'todo added successfully',
      updatedTodo,
    });    
});

const PORT = 5000;

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
});
