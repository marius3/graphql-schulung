const tableBody = document.getElementById('tableBody');
let adapter = restAdapter;

function fillTable(response) {

    tableBody.innerHTML = '';
    const json = JSON.parse(response);
    let todos = json.data ? json.data.todos : json;

    if (todos.data) {
        todos = todos.data.todos;
    }

    if (!(todos instanceof Array)) {
        todos = [todos];
    }

    todos.forEach(todo => {
        createTableEntry(todo);
    });
}

function createTableEntry(todo) {

    let formattedPriority = formatValue(todo.priority);
    let formattedDifficulty = formatValue(todo.difficulty);
    let formattedStatus = formatValue(todo.status);

    let id = todo.id;

    tableBody.innerHTML += `<tr id="tr${id}" class="dataRow">
        <td>${id}</td>
        <td>${todo.title}</td>
        <td>${todo.description}</td>
        <td>${formattedPriority}</td>
        <td>${formattedDifficulty}</td>
        <td>${formattedStatus}</td>
        <td>
            <i class="material-icons edit mx-1" id="editTodo${id}">
                edit
            </i>
            <i class="material-icons delete mx-1" id="removeTodo${id}">
                clear
            </i>
        </td>
    </tr>

    <tr id="edit${id}" class="editRow hidden">
        <td>${id}</td>
        <td><input type="text" class="form-control" id="tr${id}-edit-title" value="${todo.title}"></td>
        <td><input type="text" class="form-control" id="tr${id}-edit-description" value="${todo.description}"></td>
        <td>
            <select class="form-control" id="tr${id}-edit-priority">
                <option class="priority-urgent">urgent</option>
                <option class="priority-high">high</option>
                <option class="priority-medium">medium</option>
                <option class="priority-low">low</option>
            </select>
        </td>
        <td>
            <select class="form-control" id="tr${id}-edit-difficulty">
                <option class="difficulty-hard">hard</option>
                <option class="difficulty-normal">normal</option>
                <option class="difficulty-easy">easy</option>
            </select>
        </td>
        <td>
            <select class="form-control" id="tr${id}-edit-status">
                <option class="status-todo">todo</option>
                <option class="status-in-progress">in progress</option>
                <option class="status-done">done</option>
            </select>
        </td>
        <td>
            <button type="button" class="btn btn-primary btn-sm" onclick="updateEntry(${id})">Update</button>
            <button type="button" class="btn btn-danger btn-sm" onclick="cancelEdit(${id})">Cancel</button>
        </td>
    </tr>`;

    document.getElementById(`editTodo${id}`)
        .setAttribute('onclick', `editTodo(${id}, '${todo.priority}', '${todo.difficulty}', '${todo.status}')`);
    document.getElementById(`removeTodo${id}`)
        .setAttribute('onclick', `removeTodo(${id})`);
}

function removeTodo(id) {
    adapter.deleteTodo(id);
}

function toDisplay(enumElement) {
    return enumElement.toLowerCase().replace(/_/g, " ");
}
function toEnum(text) {
    return text.toUpperCase().replace(/ /g, "_");
}

function editTodo(id, priority, difficulty, status) {
    closeOtherEditFields(id);
    document.getElementById(`tr${id}`).classList.add('hidden');
    document.getElementById(`edit${id}`).classList.remove('hidden');
    document.getElementById(`tr${id}-edit-priority`).value = toDisplay(priority);
    document.getElementById(`tr${id}-edit-difficulty`).value = toDisplay(difficulty);
    document.getElementById(`tr${id}-edit-status`).value = toDisplay(status);
}

function closeOtherEditFields(id) {
    let editRows = document.getElementsByClassName('editRow');
    let dataRows = document.getElementsByClassName('dataRow');

    for (let row of editRows) {
        if (row.id !== id) {
            row.classList.add('hidden');
        }
    }

    for (let row of dataRows) {
        if (row.id !== id) {
            row.classList.remove('hidden');
        }
    }
}

function cancelEdit(id) {
    document.getElementById(`tr${id}`).classList.remove('hidden');
    document.getElementById(`edit${id}`).classList.add('hidden');
}

function formatValue(value) {
    let color;

    switch (value) {
        case 'URGENT':
        case 'HARD':
        case 'TODO':
            color = '#FF0000';
            break;
        case 'HIGH':
        case 'NORMAL':
        case 'IN_PROGRESS':
            color = '#FFA500';
            break;
        case 'MEDIUM':
        case 'EASY':
        case 'DONE':
            color = '#008000';
            break;
        case 'LOW':
            color = '#0000FF';
            break;
        default:
            color = '#000000';
            break;
    }

    return `<span style="color: ${color}; font-weight: bold">${toDisplay(value)}</span>`;
}

function closeCreateTodoForm() {
    document.getElementById('createNewTodoForm').classList.remove('show');
}

function showError(message) {
    showToast(message, 'danger');
}

function showSuccess(message) {
    showToast(message, 'success');
}

function showToast(message, alertType) {
    messageContainer.innerHTML = `
        <div class="alert alert-${alertType} toast-message" role="alert">
            ${message}
        </div>`;
}

function setAdapter(newAdapter) {

    closeCreateTodoForm();
    let settings = [dummyAdapter, '', ''];
    
    if (newAdapter === 'REST') {
        settings = [restAdapter, 'underline', ''];
    } else if (newAdapter === 'GraphQL') {
        settings = [graphQlAdapter, '', 'underline'];
    }

    adapter = settings[0]
    document.getElementById('title-rest').style.textDecoration = settings[1];
    document.getElementById('title-gql').style.textDecoration = settings[2];
}

function updateEntry(id) {

    const title = document.getElementById(`tr${id}-edit-title`).value;
    const description = document.getElementById(`tr${id}-edit-description`).value;
    const priority = toEnum(document.getElementById(`tr${id}-edit-priority`).value);
    const difficulty = toEnum(document.getElementById(`tr${id}-edit-difficulty`).value);
    const status = toEnum(document.getElementById(`tr${id}-edit-status`).value)

    let todo = JSON.parse(`{ "title": "${title}", "description": "${description}", "priority": "${priority}", "difficulty": "${difficulty}", "status": "${status}" }`);

    for (let prop in todo) {
        if (!todo[prop]) {
            showInvalidInputError(prop);
            return;
        }
    }

    adapter.putTodo(id, todo);
}

function showAll() {
    closeCreateTodoForm();
    adapter.getTodos();
}

function createNewTodo() {

    const title = document.getElementById("todoTitle").value;
    const description = document.getElementById("todoDescription").value;
    const priority = toEnum(document.getElementById("todoPriority").value);
    const difficulty = toEnum(document.getElementById("todoDifficulty").value);
    const status = toEnum(document.getElementById("todoStatus").value);

    let todo = JSON.parse(`{ "title": "${title}", "description": "${description}", "priority": "${priority}", "difficulty": "${difficulty}", "status": "${status}" }`);

    for (let key of Object.keys(todo)) {
        if (!todo[key]) {
            let message = `Field '${key}' must not be empty.`;
            showError(message);
            return;
        }
    }

    adapter.postTodo(todo);
}

function filterBy(key, value) {
    closeCreateTodoForm();
    adapter.query(key, toEnum(value));
}

function search() {
    const searchText = document.getElementById('searchInput').value;
    filterBy('search', searchText);
}

adapter.getTodos();
